from os import path
import ConfigParser

def getConfig():
    c = ConfigParser.ConfigParser()
    c.read(path.join(path.dirname(path.realpath(__file__)), 'config.ini'))
    return c

conf = getConfig()
