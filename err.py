import json
from flask import Response

class ErrRes(Exception):
    def __init__(self, e, *a, **k):
        self.response = {"error": e}
        super(ErrRes, self).__init__(e, *a, **k)

class JSONRequestHandler:
    def __init__(self, fn):
        self.fn = fn
    
    def __call__(self, *a, **k):
        try:
            # Instead of dumping json in every
            # view function, we do it here
            r = self.fn(*a, **k)
            if isinstance(r, Response):
                return r
            resp = json.dumps(r)
        except ErrRes, e:
            resp = json.dumps(e.response)
        return Response(resp, mimetype='application/json')
    
    def __getattr__(self, attr):
        return getattr(self.fn, attr)
