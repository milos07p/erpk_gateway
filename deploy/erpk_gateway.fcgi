#!/usr/bin/python
import sys
sys.path.insert(0, '/path/to/app')

from flup.server.fcgi import WSGIServer
from core import app

if __name__ == '__main__':
    WSGIServer(app).run()
