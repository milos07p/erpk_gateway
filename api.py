import json
import urllib2

import datasources as ds
from conf import conf
from err import ErrRes

# No idea how bad this really is (params' default being a list)
# because the default one is kept in future (say we append something to
# it inside the fn, it'll stay on the next fn call
def _getResource(path, params=[], cache=False):
    path = path.format(*map(urllib2.quote, params))
    res = ds.mcGet(path)
    if res:
        return res
    try:
        res = urllib2.urlopen(conf.get('erpk', 'baseurl') + path)
        data = json.load(res)
        res.close()
        if cache:
            ds.mcSet(path, data, time=cache)
        return data
    except urllib2.URLError, e:
        try:
            res = json.loads(e.read())
            if not res.get('type'):
                raise
            elif res['type'] == b"Erpk\Harvester\Module\Citizen\Exception\CitizenNotFoundException":
                raise ErrRes('CITIZEN_NOT_FOUND')
            elif res['type'] == b"Erpk\Harvester\Module\Military\Exception\UnitNotFoundException":
                raise ErrRes('UNIT_NOT_FOUND')
            elif res['type'] == b"Erpk\Harvester\Module\Military\Exception\CampaignNotFoundException":
                raise ErrRes('CAMPAIGN_NOT_FOUND')
        except ValueError:
            pass
        raise ErrRes('SERVICE_TEMPORARY_UNAVAILABLE')
    except urllib2.HTTPError, e:
        if e.read() == '':
            raise ErrRes('NOT_FOUND')
        raise

def citizen(id):
    data = _getResource('/citizen/profile/{0}.json', (str(id),), cache=1800)
    ds.dbFeedCitizen(data['name'], data['id'], data['birth'])
    if data['military']['unit']:
        ds.dbFeedUnit(data['military']['unit']['name'].strip(),
                      data['military']['unit']['id'])
    if data['party']:
        ds.dbFeedParty(data['party']['name'], data['party']['id'])
    if data['newspaper']:
        ds.dbFeedNewspapers(data['newspaper']['name'], data['newspaper']['id'])
    return data

def citizenSearch(query):
    data = _getResource('/citizen/search/{0}/1.json', (query,), cache=False)
    target = None
    for c in data:
        ds.dbFeedCitizen(c['name'], c['id'])
        if c['name'].lower() == query.lower():
            target = c
    return target

def unit(id):
    data = _getResource('/unit/{0}.json', (str(id),), cache=21600)
    ds.dbFeedUnit(data['name'].strip(), data['id'])
    return data

def battle(id):
    return _getResource('/battle/{0}.json', (str(id),), cache=30)

def market(country, industry, quality):
    return _getResource('/market/{0}/{1}/{2}/1.json', (country, industry, quality,), cache=300)

def exchange(mode):
    return _getResource('/exchange/{0}/1.json', (mode,), cache=600)
