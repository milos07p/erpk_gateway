#!/usr/bin/env python2
from hashlib import md5

from conf import conf
import api
import datasources as ds
from err import ErrRes, JSONRequestHandler


# app views below

from flask import Flask
app = Flask(__name__)
if conf.getboolean('core', 'debug'):
    app.debug = True


@app.route("/")
def hello():
    return "erpk(harserver) gateway - Get yours today! http://bitbucket.org/Digital_Lemon/erpk_gateway"

@app.route("/resolve/<query>")
@JSONRequestHandler
def resolve(query):
    try:
        data = ds.dbGetCitizen(query)
        data['birth'] = data['birth'].strftime("%Y-%m-%d")
        data['avatar'] = 'http://static.erepublik.net/uploads/avatars/Citizens/%s/%s.jpg' % \
                          (data['birth'].replace('-', '/'), md5(str(data['id'])).hexdigest())
        try:
            data['last_updated'] = data['last_updated'].strftime("%Y-%m-%d %H:%M:%S")
        except AttributeError:
            data['last_updated'] = None
        return data
    except TypeError:
        raise ErrRes('NOT_FOUND')

@app.route("/citizen/name/<query>")
@JSONRequestHandler
def citizenByName(query):
    try:
        # Test!
        c = ds.dbGetCitizen(query) or \
            api.citizenSearch(query)
        c = c['id']
    except TypeError:
        raise ErrRes('NOT_FOUND')
    if c:
        data = api.citizen(c)
        # ds.dbFeedCitizen(data['name'], data['id'], data['birth'])
        return data

@app.route("/citizen/id/<query>")
@JSONRequestHandler
def citizenById(query):
    return api.citizen(query)

@app.route("/unit/name/<query>")
@JSONRequestHandler
def unitByName(query):
    try:
        u = ds.dbGetUnit(query)['id']
    except TypeError:
        raise ErrRes('NOT_FOUND')
    if u:
        return api.unit(u)

@app.route("/unit/id/<query>")
@JSONRequestHandler
def unitById(query):
    return api.unit(query)

@app.route("/party/name/<query>")
@JSONRequestHandler
def partyByName(query):
    p = ds.dbGetParty(query)
    if not p:
        raise ErrRes('NOT_FOUND')
    return p

@app.route("/newspapers/name/<query>")
@JSONRequestHandler
def newspapersByName(query):
    n = ds.dbGetNewspapers(query)
    if not n:
        raise ErrRes('NOT_FOUND')
    return n

@app.route("/battle/<query>")
@JSONRequestHandler
def battle(query):
    return api.battle(query)

@app.route("/market/<country>/<industry>/<quality>")
@JSONRequestHandler
def market(**query):
    return api.market(**query)



if __name__ == '__main__':
    app.run(host='0.0.0.0')
