import pymysql
import memcache
from urllib2 import quote
from datetime import date
from object_pool import ObjectPool

from conf import conf


def makeDb():
    params = {
        'host': conf.get('database', 'host'),
        'user': conf.get('database', 'user'),
        'passwd': conf.get('database', 'passwd'),
        'db': conf.get('database', 'db'),
        'unix_socket': conf.get('database', 'sock'),
    }
    db = pymysql.connect(**params)
    db.autocommit(True)
    cur = db.cursor(pymysql.cursors.DictCursor)
    return (db, cur)

def makeMc():
    params = (
              conf.get('memcached', 'host'),
              conf.get('memcached', 'port'),
             )
    return (memcache.Client([':'.join(params)]), conf.get('memcached', 'keyprefix'))


db_pool = ObjectPool(makeDb, max_size=conf.getint('database', 'poolsize'), reset=lambda x: x[0].ping(True))
mc_pool = ObjectPool(makeMc, max_size=conf.getint('memcached', 'poolsize'))

# init tables
with db_pool.item() as (db, cur):
    cur.execute("""CREATE TABLE IF NOT EXISTS `citizens` (
                     `id` mediumint(8) unsigned NOT NULL,
                     `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                     `birth` date NOT NULL,
                     `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                     PRIMARY KEY (`id`),
                     KEY `name` (`name`(6))
                   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci""")
    cur.execute("""CREATE TABLE IF NOT EXISTS `units` (
                     `id` mediumint(8) unsigned NOT NULL,
                     `name` varchar(200) NOT NULL,
                     `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     PRIMARY KEY (`id`)
                   ) ENGINE=InnoDB DEFAULT CHARSET=utf8""")
    cur.execute("""CREATE TABLE IF NOT EXISTS `newspapers` (
                     `id` mediumint(8) unsigned NOT NULL,
                     `name` varchar(200) NOT NULL,
                     `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     PRIMARY KEY (`id`)
                   ) ENGINE=InnoDB DEFAULT CHARSET=utf8""")
    cur.execute("""CREATE TABLE IF NOT EXISTS `parties` (
                     `id` mediumint(8) unsigned NOT NULL,
                     `name` varchar(200) NOT NULL,
                     `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                     PRIMARY KEY (`id`)
                   ) ENGINE=InnoDB DEFAULT CHARSET=utf8""")


def dbGetCitizen(name):
    with db_pool.item() as (db, cur):
        cur.execute("SELECT name, id, birth, last_updated FROM citizens WHERE name=%s", (name,))
        if cur.rowcount:
            return cur.fetchone()

def dbFeedCitizen(name, id, birth="1970-01-01"):
    birth = map(int, birth.split('-'))
    with db_pool.item() as (db, cur):
        try:
            cur.execute("""INSERT INTO citizens (name, id, birth)
                       VALUES (%s, %s, %s)""", (name, id, date(*birth),))
        except pymysql.err.IntegrityError:
            # Worst ever?
            # TODO: Test!
            cur.execute("""UPDATE citizens SET name=%s,
                        last_updated=CURRENT_TIMESTAMP WHERE id=%s AND
                        birth != '0000-00-00'""", (name, id,)) \
            or \
            cur.execute("""UPDATE citizens SET name=%s, birth=%s,
                        last_updated=CURRENT_TIMESTAMP WHERE id=%s""",
                        (name, date(*birth), id,))

def dbGetUnit(name):
    with db_pool.item() as (db, cur):
        cur.execute("SELECT name, id FROM units WHERE name=%s", (name,))
        if cur.rowcount:
            return cur.fetchone()

def dbFeedUnit(name, id):
    with db_pool.item() as (db, cur):
        try:
            cur.execute("""INSERT INTO units (name, id)
                       VALUES (%s, %s)""", (name, id,))
        except pymysql.err.IntegrityError:
            cur.execute("""UPDATE units SET name=%s,
                        last_updated=CURRENT_TIMESTAMP WHERE id=%s""",
                        (name, id,))

def dbGetNewspapers(name):
    with db_pool.item() as (db, cur):
        cur.execute("SELECT name, id FROM newspapers WHERE name=%s", (name,))
        if cur.rowcount:
            return cur.fetchone()

def dbFeedNewspapers(name, id):
    with db_pool.item() as (db, cur):
        try:
            cur.execute("""INSERT INTO newspapers (name, id)
                       VALUES (%s, %s)""", (name, id,))
        except pymysql.err.IntegrityError:
            cur.execute("""UPDATE newspapers SET name=%s,
                        last_updated=CURRENT_TIMESTAMP WHERE id=%s""",
                        (name, id,))

def dbGetParty(name):
    with db_pool.item() as (db, cur):
        cur.execute("SELECT name, id FROM parties WHERE name=%s", (name,))
        if cur.rowcount:
            return cur.fetchone()

def dbFeedParty(name, id):
    with db_pool.item() as (db, cur):
        try:
            cur.execute("""INSERT INTO parties (name, id)
                       VALUES (%s, %s)""", (name, id,))
        except pymysql.err.IntegrityError:
            cur.execute("""UPDATE parties SET name=%s,
                        last_updated=CURRENT_TIMESTAMP WHERE id=%s""",
                        (name, id,))


def mcGet(key):
    with mc_pool.item() as (mc, keyprefix):
        return mc.get('%s:%s' % (keyprefix, quote(str(key)),))

def mcSet(key, value, time=0):
    with mc_pool.item() as (mc, keyprefix):
        return mc.set('%s:%s' % (keyprefix, quote(str(key)),), value, time=time)
